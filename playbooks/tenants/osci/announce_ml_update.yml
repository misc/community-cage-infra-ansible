---

- name: "Tenants Contacts"
  hosts: openshift_controller
  gather_facts: no
  module_defaults:
    group/kubernetes.core.k8s:
      api_key: "{{ openshift_dedicated_api_key }}"
      host: "{{ openshift_dedicated_host }}"
  vars:
    data_dir: "{{ inventory_dir }}/data/tenants/osci/mail"
    # OSCI Announce
    listmonk_list_id: 3
  tasks:
    - name: "Warn if tenant data is missing"
      fail:
        msg: "Tenant info for tenant '{{ item }}' are missing"
      when: "'tenant' not in hostvars[groups[item] | first]"
      loop: "{{ groups.keys() | select('match', 'tenant_') | list }}"
    - name: "Warn if tenant data does not contain contact info"
      fail:
        msg: "Tenant info for tenant '{{ item }}' do not contain contacts info"
      when: "'contacts' not in hostvars[groups[item] | first].tenant"
      loop: "{{ groups.keys() | select('match', 'tenant_') | list }}"

    # - name: "Generate new list of tenants info"
    #   set_fact:
    #     osci_tenants_info: "{{ groups.keys() | select('match', 'tenant_') | map('extract', groups) | map('first') | map('extract', hostvars) | map(attribute='tenant') | map(attribute='contacts') | selectattr('match', 'mailto:') | map('dict2items') | flatten }}"
    - name: "Generate new list of tenants info"
      set_fact:
        osci_tenants_info: "{{ groups.keys() | select('match', 'tenant_') | map('extract', groups) | map('first') | map('extract', hostvars) | map(attribute='tenant') | map(attribute='contacts') | map('dict2items') | flatten }}"
    - name: "Generate new list of tenants {emails)"
      set_fact:
        osci_announce_tenants_emails: "{{ osci_tenants_info | map(attribute='value') | flatten | select('match', 'mailto:') | map('replace', 'mailto:', '') }}"
    - name: "Generate new list of tenants (names)"
      set_fact:
        osci_announce_tenants_names: "{{ osci_tenants_info | map(attribute='key') | flatten }}"
    - name: "Generate tenants email<->name association"
      set_fact:
        osci_tenants_email_name: "{{ dict(osci_announce_tenants_emails | zip(osci_announce_tenants_names)) }}"
    - name: "Generate new list of unique new subscribers"
      set_fact:
        osci_announce_subscribers_new: "{{ osci_announce_tenants_emails | sort | unique }}"
    # - name: "New subscribers email list"
    #   debug:
    #     msg: "{{ osci_announce_subscribers_new }}"
    # - name: "Tenants email<->name association"
    #   debug:
    #     msg: "{{ osci_tenants_email_name }}"

    - name: "Fetch listmonk secrets"
      include_role:
        name: openshift/get_secret
      vars:
        project: prod-osci-announce
        secret_name: listmonk

    - name: "Fetch list of subscribers"
      uri:
        # the doc contains /api/subscribers/lists/{list_id} but that only leads to 404
        url: "https://announce.osci.io/api/subscribers?list_id={{ listmonk_list_id }}&per_page=all"
        user: listmonk
        password: "{{ openshift_secret['listmonk-admin-password'] }}"
        method: GET
        headers:
          Content-Type: "application/json;charset=utf-8"
        body_format: json
        status_code:
          - 200
      register: listmonk_response
    - name: "Compute current subscribers list"
      set_fact:
        osci_announce_subscribers_cur: "{{ listmonk_response.json.data.results | map(attribute='email') }}"
    # - name: "Current subscribers list"
    #   debug:
    #     msg: "{{ osci_announce_subscribers_cur }}"
    - name: "Compute email<->id association"
      set_fact:
        subscribers_email_id: "{{ dict(listmonk_response.json.data.results | map(attribute='email') | zip(listmonk_response.json.data.results | map(attribute='id'))) }}"
    - name: "Email<->id association"
      debug:
        msg: "{{ subscribers_email_id }}"

    - name: "Compute list of subscribers to add"
      set_fact:
        subscribers_to_add: "{{ osci_announce_subscribers_new | difference(osci_announce_subscribers_cur) }}"
    - name: "Compute list of subscribers to remove"
      set_fact:
        subscribers_to_remove: "{{ osci_announce_subscribers_cur | difference(osci_announce_subscribers_new) }}"
    - name: "List of subscribers to add"
      debug:
        msg: "{{ subscribers_to_add }}"
    - name: "List of subscribers to remove"
      debug:
        msg: "{{ subscribers_to_remove }}"

    - name: "Remove obsolete subscribers"
      uri:
        url: "https://announce.osci.io/api/subscribers/{{ subscribers_email_id[item] }}"
        user: listmonk
        password: "{{ openshift_secret['listmonk-admin-password'] }}"
        method: DELETE
        headers:
          Content-Type: "application/json;charset=utf-8"
        body_format: json
        status_code:
          - 200
      register: listmonk_response
      loop: "{{ subscribers_to_remove }}"

    - name: "Add new subscribers"
      uri:
        url: "https://announce.osci.io/api/subscribers"
        user: listmonk
        password: "{{ openshift_secret['listmonk-admin-password'] }}"
        method: POST
        headers:
          Content-Type: "application/json;charset=utf-8"
        body_format: json
        # passing parameters directly unfortunately results in them being stringified
        # which breaks the API because it expects an interger for the list ID
        body: "{{ lookup('ansible.builtin.template', data_dir + '/subscriber_info.json') }}"
        status_code:
          - 200
      register: listmonk_response
      loop: "{{ subscribers_to_add }}"


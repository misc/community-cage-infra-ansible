#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# PYTHON_ARGCOMPLETE_OK

import sys
from ansible.module_utils._text import to_text
from ansible import context
from ansible.cli import CLI
from ansible.cli.arguments import option_helpers as opt_help
from ansible.errors import AnsibleError
from ansible.plugins.loader import get_all_plugin_loaders
from ansible.vars.hostvars import HostVars
from ansible.utils.display import Display
import tempfile
import subprocess


__version__ = "0.0.1"


display = Display()


class IpmiconsoleCLI(CLI):
    name = "ansible-ipmiconsole"

    def init_parser(self):
        super().init_parser(
            usage="%prog [<host-pattern>] [options]",
            desc="Connect to host's IPMI console.",
            epilog="Opens a dedicated Java console.",
        )
        # opt_help.add_runas_options(self.parser)
        opt_help.add_inventory_options(self.parser)
        # opt_help.add_connect_options(self.parser)
        # opt_help.add_check_options(self.parser)
        opt_help.add_vault_options(self.parser)
        # opt_help.add_fork_options(self.parser)
        # opt_help.add_module_options(self.parser)
        # opt_help.add_basedir_options(self.parser)

        self.parser.add_argument("args", metavar="pattern", help="host pattern")

    def post_process_args(self, options):
        options = super().post_process_args(options)

        display.verbosity = options.verbosity
        self.validate_conflicts(options, runas_opts=False, fork_opts=False)

        return options

    # to make the class not abstract
    def run(self):
        super().run()

        host = to_text(context.CLIARGS["args"], errors="surrogate_or_strict")

        host_hv = self.get_host_vars(host)

        # TODO: make it generic to non-blades
        if "blade" not in host_hv:
            display.error(
                "host '%s' has not blade configuration" % host, wrap_text=False
            )
            sys.exit(2)
        if "ipmi" not in host_hv["blade"]:
            display.error(
                "blade '%s' has not IPMI configuration" % host, wrap_text=False
            )
            sys.exit(2)

        ipmi_info = host_hv["blade"]["ipmi"]
        if "ip" not in ipmi_info:
            display.error(
                "blade '%s' IPMI configuration does not specify IP" % host,
                wrap_text=False,
            )
            sys.exit(2)

        display.display(
            "Connecting to IPMI console on blade '%s' (IPMI IP: %s)"
            % (host, ipmi_info["ip"])
        )

        with tempfile.NamedTemporaryFile(mode="w+t") as fp_cookies:
            display.display("…getting authentication cookies")
            subprocess.check_call(
                [
                    "proxychains4",
                    "curl",
                    "--insecure",
                    "--data",
                    "name=%s" % ipmi_info["administrator"]["user"],
                    "--data",
                    "pwd=%s" % ipmi_info["administrator"]["password"],
                    "--request",
                    "POST",
                    "-c",
                    fp_cookies.name,
                    "-o",
                    "/dev/null",
                    "https://%s:443/cgi/login.cgi" % ipmi_info["ip"],
                ]
            )

            with tempfile.NamedTemporaryFile(mode="w+t") as fp_config:
                display.display("…fetching JNLP configuration")
                # Referer is used so the script knows we're on an HTTPS
                # connection and generates the proper URL in the
                # configuration
                subprocess.check_call(
                    [
                        "proxychains4",
                        "curl",
                        "--insecure",
                        "-b",
                        fp_cookies.name,
                        "-H",
                        "Referer: https://127.0.0.1/",
                        "-o",
                        fp_config.name,
                        "https://%s:443/cgi/url_redirect.cgi?url_name=ikvm"
                        "&url_type=jwsk" % ipmi_info["ip"],
                    ]
                )

                display.display("…establishing console connection")
                subprocess.check_call(["proxychains4", "javaws", fp_config.name])

    def get_host_vars(self, host):
        # dynamically load any plugins
        get_all_plugin_loaders()

        loader, inventory, variable_manager = self._play_prereqs()
        try:
            super().get_host_list(inventory, context.CLIARGS["subset"], host)
        except AnsibleError:
            display.error(
                "host '%s' not found in Ansible configuration" % host, wrap_text=False
            )
            sys.exit(2)

        hv = HostVars(inventory, variable_manager, loader)
        return hv[host]


def main(args=None):
    IpmiconsoleCLI.cli_executor(args)


if __name__ == "__main__":
    main()

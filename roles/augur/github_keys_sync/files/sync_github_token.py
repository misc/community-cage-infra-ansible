#!/usr/bin/python3
import sys
import psycopg2
# this code directly use the schema, because taht's easier
# since we deploy augur as a container, using the defined schema
# would requires complex setup to run the script inside the codebase, etc

# TODO use those constants
TOKEN_COL = 'access_token'
TABLE = 'worker_oauth'

class Database():
    def __init__(self, db_name):
        self.tokens = set()
        self.db_name = db_name

    def __enter__(self):
        self.con = psycopg2.connect("dbname=%s user=postgres" % self.db_name)
        return self

    def get_all_tokens(self):
        with self.con.cursor() as cur:
            cur.execute("SELECT access_token FROM augur_operations.worker_oauth")
            for t in cur.fetchall():
                self.tokens.add(t)

    def insert_update(self, token):
        if token not in self.tokens:
            with self.con.cursor() as cur:
                cur.execute("INSERT INTO augur_operations.worker_oauth (name, access_token, access_token_secret, consumer_key, consumer_secret)  VALUES ('ansible managed', %s, '1', '1', '1')", (token,))
                self.con.commit()

    def __exit__(self, type, value, traceback):
        self.con.close()


with open(sys.argv[1]) as f:
    with Database(db_name="augur") as d:
        for t in f.readlines():
            t = t[:-1]
            t = t.strip()
            if len(t):
                t = t.split()[0]
                if len(t):
                    if t[0] != '#':
                        d.insert_update(t)

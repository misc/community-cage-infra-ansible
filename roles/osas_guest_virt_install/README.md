# Ansible role for OSAS VM installation

## Introduction

This role wraps the 'guest_virt_install' role in order to simplify
configuration using description of the OSAS network.

The OSAS network is already defined for other purpose using
group_vars/host_vars. Most importantly the Comunity Cage VLANs are
defined in `cage_vlans`. To find the proper bridge information and
possibly other parameters, virt hosts are described into `virt`.

## Variables

- **main_iface_vlan**: name of the VLAN the main interface is connected to;
                       this role use this information to define the `bridge`
                       and `network` parameters
- **extra_ifaces_vlans**: name of the VLANs to add extra interfaces,
                       similarly as the `main_iface_vlan` parameter except
                       the network is not configured

All other parameters are passed to the 'guest_virt_install' role.

